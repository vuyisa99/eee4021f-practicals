#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_LINE_SIZE 1000
#define MAX_FIELDS 1000

void print(float array[], int s) {
    for (int i = 0; i < s; i++) {
        printf("%.2f ", array[i]);
    }
}

void swap(float *a, float *b) {
    float temp = *a;
    *a = *b;
    *b = temp;
}

void bubbleSort(float arr[], int size) {
    for (int j = 0; j < size - 1; ++j) {
        for (int i = 0; i < size - j - 1; ++i) {
            if (arr[i] > arr[i + 1]) {
                swap(&arr[i], &arr[i+1]);
            }
        }
    }
}

char ***read_csv(const char *filename, int *num_lines, int *num_fields) {
    FILE *file = fopen(filename, "r"); // Open the CSV file for reading
    if (file == NULL) {
        perror("Error opening file");
        return NULL;
    }

    char ***data = NULL; // Pointer to store the array of arrays containing the fields
    char line[MAX_LINE_SIZE]; // Buffer to store each line of the file
    char *token; // Pointer to store each token (field) extracted from the line
    int line_count = 0; // Counter for the number of lines in the file
    int num_fields_per_line; // Number of fields in each line

    // Read each line of the file
    while (fgets(line, MAX_LINE_SIZE, file) != NULL) {
        num_fields_per_line = 0;
        // Allocate memory for the array of fields for the current line
        char **fields = malloc(MAX_FIELDS * sizeof(char *));
        if (fields == NULL) {
            perror("Memory allocation failed");
            return NULL;
        }

        // Split the line into fields using strtok
        token = strtok(line, ",");
        while (token != NULL && num_fields_per_line < MAX_FIELDS) {
            // Allocate memory for the field and copy the token into it
            fields[num_fields_per_line] = strdup(token);
            // Move to the next token
            token = strtok(NULL, ",");
            num_fields_per_line++;
        }

        // Store the array of fields for the current line in the data array
        data = realloc(data, (line_count + 1) * sizeof(char **));
        if (data == NULL) {
            perror("Memory allocation failed");
            return NULL;
        }
        data[line_count] = fields;
        line_count++;
    }

    // Close the file
    fclose(file);

    // Set the number of lines and number of fields
    *num_lines = line_count;
    *num_fields = num_fields_per_line;

    return data;
}


int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Usage: %s <input_csv> <output_csv>\n", argv[0]);
        return 1;
    }

    char *input_csv = argv[1];
    char *output_csv = argv[2];

    clock_t start_load_input, end_load_input;
    clock_t start_start_mpi, end_start_mpi;
    clock_t start_process_time, end_process_time;
    clock_t start_save_results, end_save_results;
    double cpu_time_used_load_input, cpu_time_used_start_mpi, cpu_time_used_save_results, cpu_time_used_process_time;

    int num_lines, num_fields;
    start_load_input = clock(); // Record the starting time for loading the data 
    char ***data = read_csv(input_csv, &num_lines, &num_fields);
    if (data == NULL) {
        printf("Failed to read input CSV file\n");
        return 1;
    }
    end_load_input = clock(); // Record the ending time for loadind the data

    // Calculate the elapsed time for process 1
    cpu_time_used_load_input = ((double) (end_load_input - start_load_input))* 1000.0 / CLOCKS_PER_SEC;

    printf("Time elapsed for loading data: %g milliseconds\n", cpu_time_used_load_input);

    /*
        I am leaving this space to start the mpi and measure the performance/time it takes...
    */

   start_process_time = clock(); // Record the starting time for processing the data 
    // Create arrays for each column
    float columns[num_fields][num_lines];

    // Copy elements from data to columns
    for (int j = 0; j < num_fields; j++) {
        for (int i = 0; i < num_lines; i++) {
            columns[j][i] = atof(data[i][j]); // Assuming elements are integers
        }
    }

    // Sort each column
    for (int j = 0; j < num_fields; j++) {
        bubbleSort(columns[j], num_lines); // Assuming bubbleSort takes an array and its size
    }

    end_process_time = clock(); // Record the ending time for processing data
    // Calculate the elapsed time for process 1
    cpu_time_used_process_time = ((double) (end_process_time - start_process_time))* 1000.0 / CLOCKS_PER_SEC;
    printf("Time elapsed for processing data: %g milliseconds\n", cpu_time_used_process_time);


    start_save_results = clock(); // Record the starting time for saving results
    // Write sorted columns to the output CSV file
    FILE *output_file = fopen(output_csv, "w");
    if (output_file == NULL) {
        printf("Failed to open output CSV file for writing\n");
        return 1;
    }

    for (int i = 0; i < num_lines; i++) {
        for (int j = 0; j < num_fields; j++) {
            fprintf(output_file, "%g", columns[j][i]); // Write each element to file
            if (j < num_fields - 1) {
                fprintf(output_file, ",");
            }
        }
        fprintf(output_file, "\n"); // New line after each row
    }

    fclose(output_file);


    end_save_results = clock(); // Record the ending time for processing data
    // Calculate the elapsed time for process 1
    cpu_time_used_save_results = ((double) (end_save_results - start_save_results))* 1000.0 / CLOCKS_PER_SEC;
    printf("Time elapsed for saving results: %g milliseconds\n", cpu_time_used_save_results);

    // Freeing Memory
    if (data != NULL) {
        // Free each row
        for (int i = 0; i < num_lines; i++) {
            // Free each field
            for (int j = 0; j < num_fields; j++) {
                free(data[i][j]);
            }
            free(data[i]);
        }
        // Free the array of rows
        free(data);
    }

    return 0;
}
