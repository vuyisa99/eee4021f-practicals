# EEE4021F Practicals

## MPI practical
The C program that takes in a csv file containing a matrix has been written, now all that's left to do is to introduce the MPI parallelization, The program saves the results into a csv file, and also its does calculate the time performance, below is how the program should be used. 

## Usage
Assuming that you have the matrix_100.csv file in the same directory as the bubbleSort.c program you can use the following cmds to run it. if you are using VS code, ou might see an error just ignore that as it wont cause an issues
- gcc bubbleSort.c -o bubbleSort
- bubbleSort matrix_100.csv results.csv

after running them cmnd lines a results.csv file should appear in your working directory.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
