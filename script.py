import csv
from mpi4py import MPI
import numpy as np
import sys
import time

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

#To read the csv
def read_matrix_from_csv(file_path):
    matrix = []
    file = open(file_path)
    csvreader = csv.reader(file)
    for row in csvreader:
        matrix.append(row)
    file.close()
    return matrix

#bubble sort function
def bubbleSort(arr): 
    n = len(arr)
    for i in range(n-1):
        for j in range(0, n-i-1):
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]
                
#To convert multidimensional string array into floats
def string_to_float(stringArray):
    
    #since the matrix is nxn, rows and columns are the same. So we can use len() of array
    rows = len(stringArray)
    columns = len(stringArray[0])

    for row in range(rows):
        for column in range(columns): 
            stringArray[row][column] = float(stringArray[row][column])
            
    return stringArray
         
#To create the column arrays
def create_column_arrays(matrix):
    
    rows = len(matrix)
    columns = len(matrix[0])
    column_array = [[0] * columns for i in range(columns)]
    for r in range(rows):
        for c in range(columns):
            column_array[r][c] = matrix[c][r]
    return column_array

#To reorganise the sorted colums
def reorganise_matrix(matrix):
    
    rows = len(matrix)
    columns = len(matrix[0])
    reorganised_matrix = [[0] * columns for i in range(rows)]
    for i in range(rows):
        for j in range(columns):
            reorganised_matrix[i][j] = matrix[j][i]
            
    return reorganised_matrix

#To write data to a csv file
def write_matrix_to_csv(matrix, filename):
    with open(filename, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(matrix)
        

if __name__ == "__main__":
    
    if len(sys.argv) != 3:
        print("Usage: python script_name.py input_file output_file")
        sys.exit(1)
        
    file_path = sys.argv[1]
    dest_path = sys.argv[2]
    
    timer_start = time.time()
    matrix = read_matrix_from_csv(file_path)
    timer_stop_loading = time.time()
    elapsed_loading_time = timer_stop_loading - timer_start
    print("Elapsed time: {:.6f} seconds".format(elapsed_loading_time))
    arr = string_to_float(matrix)
    column_array = create_column_arrays(arr)
    
    
    #In rank 1 we load the data and send chunks to other processes for sorting
    if rank == 0:
        print("Hello I am ", rank)
        print("Loading the data...\n")
        

        #sending the data to each process 
        multiply_factor = int(len(column_array) / 10); 
        comm.send(column_array[0:3*multiply_factor], dest=1, tag=10)
        print("Sending data to process 1\n")
        comm.send(column_array[3*multiply_factor:2*3*multiply_factor], dest=2, tag=11)
        print("Sending data to process 2\n")
        comm.send(column_array[2*3*multiply_factor:], dest=3, tag=12)
        print("Sending data to process 3\n")
        
        #recieving data from each prcess
        data1 = comm.recv(source=1, tag=20)
        data2 = comm.recv(source=2, tag=21)
        data3 = comm.recv(source=3, tag=22)
        print("All data recieved at 0")
        
        sorted_columns = []
        for i in data1:
            sorted_columns.append(i)
        for i in data2:
            sorted_columns.append(i)
        for i in data3:
            sorted_columns.append(i)
        
        organised_matrix = reorganise_matrix(sorted_columns)
        if organised_matrix:
            time_for_processing = time.time()
            elapsed_processing_time = time_for_processing - timer_start
            print("Elapsed time for processing: {:.6f} seconds".format(elapsed_processing_time))
            
        
        write_matrix_to_csv(organised_matrix, dest_path)
        time_for_saving = time.time()
        elapsed_saving_time = time_for_saving - timer_start
        print("Elapsed time for saving: {:.6f} seconds".format(elapsed_saving_time))
        
        
        
    elif rank == 1:
        data = comm.recv(source=0, tag=10)
        print("Hello from ", rank)
        print()
        print("bubble sorting at 1")
        for columns in data:
            bubbleSort(columns)
            
        comm.send(data, dest=0, tag=20)
        print("sending sorted data from 1 to 0")
        

    elif rank == 2: 
        data = comm.recv(source=0, tag=11)
        print("Hello I am ", rank)
        print()
        print("bubble sorting at 2")
        for columns in data:
            bubbleSort(columns)
            
        comm.send(data, dest=0, tag=21)
        print()
        print("sending sorted data from 2 to 0")
        
    elif rank == 3: 
        data = comm.recv(source=0, tag=12)
        print("Hello I am ", rank)
        print("bubble sorting at 3")
        for columns in data:
            bubbleSort(columns)
            
        comm.send(data, dest=0, tag=22)
        print("Sending sorted data from 3 to 0 ")
            

